FROM alpine:latest

RUN apk update \
    && apk upgrade \
    && apk add --update \
    openvpn \
    && rm -rf /var/cache/apk/

ENV OVPN_CONFIG_FILE=config.ovpn

VOLUME [ "/etc/openvpn/" ]

CMD [ "sh", "-c", "openvpn --config /etc/openvpn/${OVPN_CONFIG_FILE}" ]
